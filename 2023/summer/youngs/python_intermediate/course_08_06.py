
# pytonda diziler - array
# list
numbers = [1, 2, 3]
print(id(numbers))
numbers[1] = 55
print(id(numbers))

 # tuple
numbers_tuple = (1,2,3)
print(numbers_tuple)
print(id(numbers_tuple))
print(numbers_tuple[1])

# tuple a listeye donustur
numbers_list = list(numbers_tuple)
print(numbers_list)
# listede degisiklik yap
numbers_list[1] = 55
print(numbers_list)
# listeyi tekrar tuple a donustur
numbers_tuple = tuple(numbers_list)
print(numbers_tuple)
print(id(numbers_tuple))

# tuple - read only

# mutable -> writable -> list, set, dictionary
# immutable -> read only ->
#           integer, float, boolean, string, tuple

 # set
 # dictionary