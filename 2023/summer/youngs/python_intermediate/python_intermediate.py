# # int - integer - tamsayi
# # float - float - ondalikli sayi
# # str - string - yazi
# # bool - boolean - mantiksal deger
# #
# # a = 11
# # b = 3
# # print(a+b) #sum
# # print(a-b)
# # print(a*b)
# # print(a/b) # division
# #
# # print(a//b) # integer division / floor division
# # print(a**4) # us - kuvvet - power operator
# # print(a%b) # mod
# #
# # c = True
# # d = False
# # e = True
# # # logical operators - mantiksal operators
# # # and (&) - ve -
# # # or (|) - veya
# # # not - degil (Tersi)
# #
# # # naming conventions
# # pinarin_bir_degiskeni = 5 # snake case
# # pinarinBirDegiskeni = 5 # camel case
# # PinarinBirDegiskeni = 6 # pascal case
# # # _ ve harf disinda baslanmamali
# #
# # # tip donusturme
# # a = "4"
# # b = 2
# #
# # a_int = int(a) # type casting - type conversion - tip donusturme
# # print(a_int + b)
# #
# # b_str = str(b)
# # print(a + b_str)
# #
# # print(float(b))
#
#
# #--------------------------------------
# # 07-29-2023
# #--------------------------------------
# # relational - comparison operators
# a = 10
# b = 10
# # print(a == b)
# # print(a != b)
# # print(a < b)
# # print(a <= b)
# # print(a > b)
# # print(a >= b)
# a = 10
# b = 10
# if a <= b:
#     print("kucuk")
# elif a == b:
#     print("esit")
# else:
#     print("buyuk")
#
# age = 10
# name = "Hafsa"
# if age < 18:
#     if name == "Hafsa":
#         print("sat")
#     else:
#         print("satma")
# elif age > 18:
#     print("sat")
#
# # bir trafik polisinin ceze yazma yaziliminin bir kismini yazacaksiniz:
# # 1 - memurdan hiz degerini alin
# # 2 - 0-100 -> 0 lira
# #       100-150 -> 250 lira
# #       150-200 -> 750 lira
# #       Daha fazla 1500
# # 3 - en sonuna odeyecegiz  cezayi ekrana yazdirin
#
# hiz = int(input("Hiz degerini giriniz: "))
# ceza = 0
# if 0 <= hiz < 100:
#     ceza = 0
# elif 100 <= hiz < 150:
#     ceza = 250
# elif 150 <= hiz < 200:
#     ceza = 750
# print(ceza)

# from random import randint
# random_number = randint(0,100)
# print(random_number)
#
# # matematikte uzaklik - mutlak deger - absolute value
# # 5 - 10 -> 5 => -5
# # 10 - 5 -> 5 => 5
# num = -5
# print(abs(num))

