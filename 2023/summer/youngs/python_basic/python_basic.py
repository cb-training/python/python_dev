# a = 11
# b = 3
#
# print(a/b)
# print(a//b) # integer division - tamsayi bolme
#
# print(a * 2)
# print(a ** 4) # power operator, us alma, kuvvetini aliyor
#
#
# # bir sayinin karekokunu us operatoru ile bulunuz
# c = 9 * 9 # 9
# d = 81 ** 0.5 # ** operatoru ile
# print(d)
# #
# # # tip donusturme - type casting (conversion)
# # # integer - int - tamsayi - 5
# # # float - float - ondalikli sayi - 3.14
# # # string - str - yazi - "safasd" 'asfasdfads'
# # # boolean - bool - mantiksal degerler - True/False
# # numara = "5"
# # numara2 = 3
# # print(int(numara) + numara2)
# # print(numara + str(numara2))
#
# # user input - kullanici veri  girisi
# # adi = input("Adinizi yaziniz: ")
# # selamlama = "Merhaba " + adi
# # print(selamlama)
#
#
# # a = int(input("Bir sayi giriniz: "))
# # b = int(input("Baska bir sayi giriniz: "))
# #
# # print(a+b)
# # print(a-b)
# # print(a*b)
# # print(a/b)
# # print(a//b)
# # print(a**b)
#
# # implicit - otomatik
# # explicit - manuel
#
# a = 4
# b = 2
# print(a / b)
#
# print(a)
# print(float(a))
#
# # boolean (logical) operators
# a = True
# b = False
# # and - ve - herhangi biri false ise cevap false
# print(a and b and a) # hepsi de true ise oluyor.
# # or - veya - herhangi biri true ise cevap true
# print(a or b)
# print(not a)
import random
# if statement - if yapilari
# if sonucu_boolean_deger_olan_cumle:
#   sarta_gore_yapilacak_is
# age = int(input("Yasinizi giriniz:"))
#
# if age < 18:
#     print("Uzgunum, bilet almaya yasiniz uygun degil")
# elif age < 30:
#     print("1. Filme girmeye yasiniz uygun, buyrun biletiniz")
# else:
#     print("2. Filme girmeye yasiniz uygun, buyrun biletiniz")
#
# print("Bitti")

# ekstra kurallar anlatilacak -> if siz else olur mu gibi


# relational/comparison operators
# print(18 == 18)
# print(18 != 18)
# print(10 < 18)
# print(18 <= 18)
# print(10 > 18)
# print(18 >= 18)

