#scope
# degiskenimizin tanimli oldugu alan, yani kullanabildigimiz yer


# 1 - global scope: program scope
# x = 1
# while x > 3:
#     y = 4
#     x += 1
# print(y)

# 2 - function scope: fonksiyon scope
#       bir degisken sadece fonksiyona ait olabilir

#####
# x = 1
# def double_inc():
#     x1 = 5
#     print(x1)
#
# double_inc()
# print(x)


#random
from random import randint

# SINIFTA 40 OGRENCI VAR.
# ptesi
# rastgele 1 ogrencisecip ona kurdele takacagim
secilen_ogrenci_no = randint(1, 40)
print(secilen_ogrenci_no)

#sali 10
# 10 ogrenci icin
for tur in range(1, 11):
    secilen_ogrenci_no = randint(1, 40)
    print(secilen_ogrenci_no)

#cars 20

#pers 30

#explicit-implicit
a = float(5)
print(a)

b = 8
c = 4
print(b/c)

