# ogrencilerin herbirinin 5 ders icin rakam notlarini tutacagim ( bir ADT (abstract data type))
# 1 - ogrenci ekleme (fonksiyon)
# 2 - ogrenciye ders ekleme (fonksiyon)
# 3 - ogrencini maximum, minimum ve not ortalamasini bulma (fonksiyon ve liste return edecek)
# 4 - Okul birincisini bulma (fonksiyon ve tuple donecek (ogrenci_bilgisi, not ortalamasi))

# 10  mat 90 fizik 98 kimya 88 biyoloji 96 tarih 89
# adem mat 90 fizik 98 kimya 88 biyoloji 96 tarih 89 - dict icerisinde dict
# alev mat 90 fizik 98 kimya 88 biyoloji 96 tarih 89 - list icerisinde dict
# aynur mat 90 fizik 98 kimya 88 biyoloji 96 tarih 89
# idris mat 90 fizik 98 kimya 88 biyoloji 96 tarih 89 - set icerisinde dict
# fatih mat 90 fizik 98 kimya 88 biyoloji 96 tarih 89 - iki ayrik ADT, biri ogrenci listesi,
#                                                         liste icin dict
# ozkan mat 90 fizik 98 kimya 88 biyoloji 96 tarih 89
cenkin_notlari = {"mat": 90, "fizik": 98, "kimya": 88, "biyoloji": 96, "tarih": 89}
ademin_notlari = {"mat": 99, "fizik": 98, "kimya": 88, "biyoloji": 96, "tarih": 89}
ogrenciler = [cenkin_notlari, ademin_notlari]
# cenk hangi indexde
ogrenci_indexi = {"cenk" : 0, "adem": 1}
index = ogrenci_indexi.get("cenk")
notlar = ogrenciler[index]
print(notlar)
#-------------------------------------------------------------------------
cenkin_notlari = {"mat": 90, "fizik": 98, "kimya": 88, "biyoloji": 96, "tarih": 89}
ademin_notlari = {"mat": 99, "fizik": 98, "kimya": 88, "biyoloji": 96, "tarih": 89}
tum_notlar = { "cenk": cenkin_notlari, "adem": ademin_notlari}
notlar = tum_notlar.get("cenk")
print(notlar)
# START

def add_student(student_no):
    # ya varsa birsey yapma
    if not student_no in school_notes:
        school_notes[student_no] = {}


school_notes = {}
print(school_notes)
add_student("cenk")
print(school_notes)
add_student("adem")
print(school_notes)

# add fonsiyonu if testini ikinci fonsyonu yazip test
# edecegiz
