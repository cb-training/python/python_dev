# a = 5.8
# b = 3.7
# print(a+b)
# print(a-b)
# print(a*b)
# print(a/b)
# print(a//b)
# print(a**2)
# print(a%b)
#
# name = "A"
# surname = "B"
# age = 10
# # string concatenation
# print(name + " " + surname + " " + str(age))



a = True
b = False

print(a and b)
print(a & b)
print(a or b)
print(a | b)
print(not a)
print("--------------------")
age = 25 # assignment operator
print(age == 25) # equality operator
print(age < 30)
print(age <= 25)
print(age > 18)
print(age >= 25)
print("--------------------")
print(age < 30 and age > 25)
print( (age < 30 and age >= 25) or age > 28)

























