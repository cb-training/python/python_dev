# numara = 1
# karesi = numara * numara
# toplanan_deger = 100 + karesi
# print(toplanan_deger)

# for degisken_adi in list_type:
#   ...
#   ...
# ... for loopa ait degil
# for num in [1,2,3,4,5,6,7,8,9,10]:
#     karesi = num * num
#     toplanan_deger = 100 + karesi
#     print(toplanan_deger)

for numInRage in range(1, 11, 4):
    karesi = numInRage * numInRage
    toplanan_deger = 100 + karesi
    print(toplanan_deger)