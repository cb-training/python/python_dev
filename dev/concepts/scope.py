
# degiskenimizin tanimli oldugu alan, yani kullanabildigimiz yer
#
#
# 1 - global scope: program scope
x = 1
# ....
print("Global: " + str(x))

while x < 3:
    y = 4
    x += 1

print("Global: " + str(y))

while x > 10:
    z = 5

#print("Global: " + str(z))


# 2 - function scope: fonksiyon scope
#       bir degisken sadece fonksiyona ait olabilir
x = 1
def double_inc():
    x = 5
    print("Function: " + str(x))

print("Global: " + str(x))
double_inc()
print("Global: " + str(x))