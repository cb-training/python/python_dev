# implicit - otomatik
# implicit conversion / casting
a = 5
b = 3
print(a / b)

# explicit - manuel
# explicit conversion/casting
c = float(a)
print(c)
print(a // b)
print(int(a / b))

