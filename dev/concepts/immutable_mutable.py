# mutable: degisebilen
number_list = [5,6,7,8,9]
print(id(number_list))
number_list.append([11,12,13]) # in-place
print(number_list)
print(id(number_list))

print("=======================================")
# immutable: degisemeyen
number_tuple = (15,16,17,18,19)
print(id(number_tuple))
added_number_tuple = number_tuple + (11,12,13)
print(number_tuple)
print(id(number_tuple))
print(added_number_tuple)
print(id(added_number_tuple))

# list mutable, tuple immutable