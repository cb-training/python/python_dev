# def ali(ekmekSayisi):
#     print("Bakkala git")
#     print(str(ekmekSayisi) + " tane ekmek al")
#     print("Seker al")
#     print("Eve gel")
#
# print("Baslangic")
# ali(2)
# ali(3)
# ali(5)

# def ali(ekmekSayisi, sekerMiktari):
#     print("Bakkala git")
#     print(str(ekmekSayisi) + " tane ekmek al")
#     print(str(sekerMiktari) + " kg seker al")
#     print("Eve gel")
#
# print("Baslangic")
# ali(2, 1)
# ali(2, 2)
# ali(2, 1.5)

# def ali(ekmekSayisi, sekerMiktari, randomArg):
#     print("Bakkala git")
#     print(str(ekmekSayisi) + " tane ekmek al")
#     print(str(sekerMiktari) + " kg seker al")
#     print("Eve gel")
#
# print("Baslangic")
# #ali(2, sekerMiktari = 1, 3) # hata positional arg , keyword argdan her zaman once gelir
# ali(2, sekerMiktari = 1) #arguman sayisi fonksiyon tanimindaki gibi olmali

def ali(ekmekSayisi, sekerMiktari, optionalArg = 5):
    print(optionalArg)
    print(str(ekmekSayisi) + " tane ekmek al")
    print(str(sekerMiktari) + " kg seker al")
    print("Eve gel")

print("Baslangic")
ali(2, 1, 3)