def harf_notu(grade):
    letter_grade = ""
    if grade < 45:
        letter_grade = "F"
    elif grade < 70:
        letter_grade = "C"
    elif grade < 85:
        letter_grade = "B"
    elif grade <= 100:
        letter_grade = "A"
    return letter_grade

yunus_grade = harf_notu(82)
print(yunus_grade)
ali_grade = harf_notu(95)
print(ali_grade)
alev_grade = harf_notu(68)
print(alev_grade)
emre_grade = harf_notu(40)
print(emre_grade)