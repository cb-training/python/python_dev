# benim telefon defterim var,
# 1 - bir fonksiyon yazin parametre olarak
#   isim ve telefon numarasi alacak ve defterime ekleyecek
# 2 - bir fonksiyon yazin: telefon defterimdeki kisilerin basina
#   "Isim:" ekleyerek liste halinde return edilsin,
#   sonra da ben o listeyi  yazdirayim

phone_book = {}

def add_to_phone_book(name, number):
    print(name + ":" + str(number))
    phone_book.update({name:number})


add_to_phone_book("CB1", 1111)
add_to_phone_book("CB2", "2222")
add_to_phone_book("CB3", "2222")
add_to_phone_book("CB4", "2222")

def modify_names_in_phone_book():
    result = []
    # iterable
    for aName in phone_book.keys():
        modifiedName = "Isim:" + aName
        result.append(modifiedName)

    return result

mNames = modify_names_in_phone_book()
print(mNames)
