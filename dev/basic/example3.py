# ogrencilerin herbirinin 5 ders icin rakam notlarini tutacagim
# ( bir ADT (abstract data type))
# 1 - ogrenci ekleme (fonksiyon)
# 2 - ogrenciye ders ekleme (fonksiyon)
# 3 - ogrencinin maximum, minimum ve not ortalamasini bulma (fonksiyon ve liste return edecek)
# 4 - Okul birincisini bulma (fonksiyon ve tuple donecek (ogrenci_bilgisi, not ortalamasi))

cenkin_notlari = {"mat": 90, "fizik": 98, "kimya": 88, "biyoloji": 96, "tarih": 89}
ademin_notlari = {"mat": 99, "fizik": 98, "kimya": 88, "biyoloji": 96, "tarih": 89}
tum_notlar = { "cenk": cenkin_notlari, "adem": ademin_notlari}
notlar = tum_notlar.get("cenk")
print(notlar)
# START

def add_student(student_no):
    # ya varsa birsey yapma
    if not student_no in school_notes:
        school_notes[student_no] = {}


school_notes = {}
print(school_notes)
add_student("cenk")
print(school_notes)
add_student("adem")
print(school_notes)

# add fonksiyonu if testini ikinci fonsyonu yazip test
# edecegiz
def add_grade_to_student(student_no, lesson_code, grade):
    add_student(student_no)
    student_grades = school_notes.get(student_no)
    student_grades[lesson_code] = grade

add_grade_to_student("alev", "mat", 60)
add_grade_to_student("alev", "fizik", 80)
add_grade_to_student("alev", "kimya", 60)
add_grade_to_student("alev", "biyoloji", 90)
add_grade_to_student("alev", "tarih", 85)
#3 - ogrencinin maximum, minimum ve not ortalamasini bulma (fonksiyon ve liste return edecek)
def student_grade_report(student_no):
    student_grades = school_notes.get(student_no)
    max_grade = max(student_grades.values())
    min_grade = min(student_grades.values())
    average_grade = sum(student_grades.values()) / len(student_grades.values())
    return [min_grade, max_grade, average_grade]

report = student_grade_report("alev")
print(report)

# 4 - Okul birincisini bulma (fonksiyon ve tuple donecek (ogrenci_bilgisi, not ortalamasi))
def school_best_student():
    result = ()
    best_sum = 0
    for student_entry in school_notes.items():
        student_sum = sum(student_entry[1].values())
        print((student_sum, student_entry))
        if student_sum > best_sum:
            best_sum = student_sum
            result =  (student_entry[0], student_sum / len(student_entry[1]))
    return result

school_notes["cenk"] = cenkin_notlari
school_notes["adem"] = ademin_notlari
best_student = school_best_student()
print(best_student)
# bir trafik polisinin aksam karakola gittiginde kime ne kadar ceza
# verdigine dair yazilim yazacagiz
# 1 - plaka ceza iliskisini tutan ADT ne olabilir
# 2 - polisin plakaya ceza yazmasini saglayan br foksiyon yaziniz
# 3 - is bitiminde plaka ve yazinlan cezayi, tumunu listeleyiniz