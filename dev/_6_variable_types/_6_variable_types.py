# integer - tamsayi
numara = 5
# float - ondalikli sayi
ondalikli_numara = 3.14
# string - yazi
yazi = "Internet"
# boolean - mantiksal_deger
mantiksal_deger = True

# kisaltmalar
# integer - int - 5
# float - float - 3.14
# string - str - "ali" 'a'
# boolean - bool - True/False
# type casting = tip donusturme
numara_in_float = float(numara)
print(numara)
print(numara_in_float)

ondalikli_numara_in_int = int(ondalikli_numara)
print(ondalikli_numara)
print(ondalikli_numara_in_int)

numara2 = int(3.9)
print(numara2)

numara_in_string = str(numara)
print(numara) # 5
print(numara_in_string) # "5", '5'

a = 3.14
a_in_str = str(a)
print(a)
print(a_in_str)

b = True
b_in_str = str(b)
print(b)
print(b_in_str)
print(bool(b_in_str))











