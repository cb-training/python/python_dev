numaralar = (3, 8 ,7, 66, 55)
print(numaralar)


# add - ekleme
# mevcut degil, once listeye cevirip ekleme yapip
# sonra tekrar tuple yapilabilir
numaralar_list = list(numaralar)
numaralar_list.append(99)
numaralar_tuple = tuple(numaralar_list)
print(numaralar_tuple)

# insert - araya koyma
# mevcut degil, eklemedeki yontem uygulanabilir

# remove - silme
# mevcut degil, eklemedeki yontem uygulanabilir

# pop - sira numarasi en buyuk olani cikar ve bana ver
# mevcut degil, eklemedeki yontem uygulanabilir
# ama en azindan sondaki elemani gormek icin:
lastElementInTuple = numaralar[len(numaralar) - 1]
print(lastElementInTuple)

#index - elemanin ilk sira numarasini soyle
numaralar = (3, 8 ,7, 66, 55)
print(numaralar)
print(numaralar.index(7))

numaralar = (3, 8 ,66, 7, 7, 55, 7)
print(numaralar)
print(numaralar.index(7))

#peki ya listede olmayan bir elemanin indexini sorarsak
#print(numaralar.index(111))

#in operatoru - eleman listede mi diye sorar
numaralar = (3, 8 ,66, 7, 7, 55, 7)
listede_mi = 7 in numaralar
print(listede_mi)
listede_mi2 = 100 in numaralar
print(listede_mi2)

#count - elemandan kac tane var sayar
numaralar = (3, 8 ,66, 7, 7, 55, 7)
print(numaralar)
print(numaralar.count(7))

#reverse - listeyi ters cevirir
# copy - kopyasini alir
# clear - temizle
# extend - genislet-ekle
# sort - siralar

# + operatoru - ekleme
numaralar = (1, 2, 3)
print(numaralar)
baska_numaralar = (4, 5, 6)
son_numaralar = numaralar + baska_numaralar
print(son_numaralar)
# peki int toplanabilir mi?
baska_numaralar = (4,)
son_numaralar = numaralar + baska_numaralar
print(son_numaralar)


# min / max / sum
numaralar = (1, 2, 3)
print(min(numaralar))
print(max(numaralar))
print(sum(numaralar))
print(len(numaralar))
"""
"""