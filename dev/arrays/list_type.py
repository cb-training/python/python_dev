bos_liste = []
print(bos_liste)
print(type(bos_liste))

sayilar = [1,2,3]
print(sayilar)

ondalikli_sayilar = [1.3,2.5,3.88]
print(ondalikli_sayilar)

isimler = ["Hamza","Ferhat","Meryem"]
print(isimler)

mantiksal_degerler = [True,True,False]
print(mantiksal_degerler)

icice_liste = [[1,2], [3,4]]
print(icice_liste)

# tipki dunyada farkli milletlerin yasamasi gibi
# farkli tiplerde ayni listede yer alabilir
farkliliklarin_listesi = [1, 5, "abc", [1,2], True]
print(farkliliklarin_listesi)

# index operator
organlar = ["kalp", "beyin", "goz", "kulak", "mide"]
# sira numarasi
#              0        1       2       3       4
print(organlar[0])
print(organlar[1])
print(organlar[2])
print(organlar[3])
print(organlar[4])
#print(organlar[5])

organlar = ["kalp", "beyin", "goz", "kulak", "mide"]
# ters sira numarasi
#              -5       -4      -3       -2      -1
print(organlar[-5])
print(organlar[-4])
print(organlar[-3])
print(organlar[-2])
print(organlar[-1])

#slicing
duyu_organlari = organlar[2:3]
print(duyu_organlari)

duyu_organlari = organlar[2:4]
print(duyu_organlari)

duyu_organlari = organlar[2:-1]
print(duyu_organlari)

alt_kume_organlar = organlar[0:3]
print(alt_kume_organlar)

alt_kume_organlar = organlar[:3]
print(alt_kume_organlar)

alt_kume_organlar = organlar[2:5]
print(alt_kume_organlar)

alt_kume_organlar = organlar[2:]
print(alt_kume_organlar)

alt_kume_organlar = organlar[:]
print(alt_kume_organlar)

