numaralar = [3, 8 ,7, 66, 55]
print(numaralar)


# add - ekleme
numaralar.append(99)
print(numaralar)


# insert - araya koyma
numaralar.insert(2, 100)
print(numaralar)

# remove - silme
numaralar.remove(8)
print(numaralar)

# ya birden fazla ayni eleman olursa
numaralar = [3, 5, 5, 5, 3, 3, 4]
print(numaralar)
numaralar.remove(5)
print(numaralar)

# pop - sira numarasi en buyuk olani cikar ve bana ver
numaralar = [3, 8 ,7, 66, 55]
print(numaralar)
print(numaralar.pop())
print(numaralar)

# pop(index) - sira numarasindakini cikar ve bana ver
numaralar = [3, 8 ,7, 66, 55]
print(numaralar)
print(numaralar.pop(3))
print(numaralar)

#index - elemanin ilk sira numarasini soyle
numaralar = [3, 8 ,7, 66, 55]
print(numaralar)
print(numaralar.index(7))

numaralar = [3, 8 ,66, 7, 7, 55, 7]
print(numaralar)
print(numaralar.index(7))

#peki ya listede olmayan bir elemanin indexini sorarsak
#print(numaralar.index(111))

#in operatoru - eleman listede mi diye sorar
numaralar = [3, 8 ,66, 7, 7, 55, 7]
listede_mi = 7 in numaralar
print(listede_mi)
listede_mi2 = 100 in numaralar
print(listede_mi2)

#count - elemandan kac tane var sayar
numaralar = [3, 8 ,66, 7, 7, 55, 7]
print(numaralar)
print(numaralar.count(7))

#reverse - listeyi ters cevirir
numaralar = [3, 8 ,66, 7, 7, 55, 7]
print(numaralar)
numaralar.reverse()
print(numaralar)

# copy - kopyasini alir
numaralar = [3, 8 ,66, 7, 7, 55, 7]
print(numaralar)
numaralar_kopya = numaralar.copy()
numaralar_kopya[1] = 777
print(numaralar)
print(numaralar_kopya)

# clear - temizle
numaralar = [3, 8 ,66, 7, 7, 55, 7]
print(numaralar)
numaralar.clear()
print(numaralar)

# extend - genislet-ekle
numaralar = [1, 2, 3]
print(numaralar)
baska_numaralar = [4, 5, 6]
numaralar.extend(baska_numaralar)
print(numaralar)

# + operatoru - ekleme
numaralar = [1, 2, 3]
print(numaralar)
baska_numaralar = [4, 5, 6]
son_numaralar = numaralar + baska_numaralar
print(son_numaralar)
# peki liste ile int toplanabilir mi?

# min / max / sum
numaralar = [1, 2, 3]
print(min(numaralar))
print(max(numaralar))
print(sum(numaralar))

# sort - siralar
numaralar = [14, 12, 132, 22]
print(numaralar)
numaralar.sort()
print(numaralar)

print(len(numaralar))
"""
"""