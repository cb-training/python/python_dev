number_set = {1,2,3}
print(number_set)

# indexing is same as list

print("--- list vs sets ---")
number_set = {1,2,3, 3, 3, 2, 2, 5, 3, 1, 8, 8, 6}
print(number_set)

print("--- add ---")
number_set = {1,2,3}
print(id(number_set))
number_set.add(4)
print(number_set)
print(id(number_set))

print("--- remove ---")
number_set = {1,2,3}
print(number_set)
number_set.remove(3)
print(number_set)
# set de olmayan bir eleman hata sebebidir
# o halde remove yerine discard kullanilabilir
number_set.discard(111)
print(number_set)

print("--- update ---")
number_set = {1,2,3}
print(number_set)
number_set.update([4, 5, 5, 5])
print(number_set)
number_set.update((6, 7))
print(number_set)
number_set.update({8, 9})
print(number_set)

# below are same in list
# pop
# clear
# copy

print("--- union ---")
set1 = {1, 2, 3}
set2 = {3, 4, 5}
union_set = set1.union(set2)
print(set1)
print(set2)
print(union_set)

print("--- intersection ---")
set1 = {1, 2, 3}
set2 = {3, 4, 5}
union_set = set1.intersection(set2)
print(set1)
print(set2)
print(union_set)

print("--- difference ---")
set1 = {1, 2, 3}
set2 = {3, 4, 5}
union_set = set1.difference(set2)
print(set1)
print(set2)
print(union_set)

print("--- symmetric_difference ---")
set1 = {1, 2, 3}
set2 = {3, 4, 5}
union_set = set1.symmetric_difference(set2)
print(set1)
print(set2)
print(union_set)