#key -value pairs
# student_no - grade pair

# List - [
# Tuple - (
# set and dictionary {

empty_dictionary = {}
print(empty_dictionary)

one_grade_dictionary = { 1234 : 100 }
print(one_grade_dictionary)
multiple_grade_dictionary = { 1234 : 100, 5678: 90 }
print(multiple_grade_dictionary)

print("--- immutable keys ---")
# immutable types: int, float, boolean, string, tuple
# mutable types list, set, dictionary
# keys must be immutable
# values can be mutable
grade_dictionary = {
    "ali": 75,
    "bahar": 85
}
print(grade_dictionary)
list_grade_dict = { "ali": [85, 90, 10]}
print(list_grade_dict)

print("--- unordered ---")
print(grade_dictionary)

print("--- read ---")
print(grade_dictionary["ali"])
#print(grade_dictionary["yigit"])
print(grade_dictionary.get("yigit"))
print(grade_dictionary.get("yigit", 90))

print("--- add/modify ---")
print(grade_dictionary)
grade_dictionary["ali"]=100
print(grade_dictionary)
grade_dictionary["ekrem"]=100
print(grade_dictionary)
grade_dictionary.update({"can": 95})
print(grade_dictionary)

print("--- pop ---")
print(grade_dictionary.popitem()) # no key
print(grade_dictionary)
print(grade_dictionary.pop("ali")) #  key
print(grade_dictionary)



print("--- in ---")
print("ali" in grade_dictionary)
print(85 in grade_dictionary)

print("--- keys-values ---")
print(grade_dictionary.keys())
print(grade_dictionary.values())
print(grade_dictionary.items())

# copy , clear, len => same as list