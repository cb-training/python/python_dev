
# if - eger
# if boolean_vaue:
#  code
# isHungry = False
# if isHungry:
#     print("Kahvalti yap")
# else: # degilse
#     print("Kahvalti yapma")

# age = 30
# if age < 18:
#     print("Bilet satma")
# else: # degilse
#     print("Bilet sat")


age = 40
if age < 18:
    print("Bilet satma")
elif age < 21:
    print("1.ye Bilet sat")
else: # degilse
    print("2.ye Bilet sat")